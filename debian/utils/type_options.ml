type options_value =
ValModule of file
| ValInt of int64
| ValFloat of float
| ValList of ( options_value list )
| ValString of string
| ValChar of string
| ValIdent of string

and options =
StringId of string * options_value
| Id of string * options_value

and file =
Comment of string * file
| Options of options * file
| Eof
