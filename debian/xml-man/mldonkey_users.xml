<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" 
"/usr/share/xml/docbook/schema/dtd/4.4/docbookx.dtd" [
  <!ENTITY debian "Debian GNU/Linux">
  <!ENTITY dhprg  "<command>mldonkey_users</command>">
]>

<!--**********************************************************************-->
<!-- Mldonkey_users manpage                                               -->
<!--                                                                      -->
<!-- Copyright (C) 2003-2006 Sylvain Le Gall <gildor@debian.org>          -->
<!--                                                                      -->
<!-- This library is free software; you can redistribute it and/or        -->
<!-- modify it under the terms of the GNU Lesser General Public           -->
<!-- License as published by the Free Software Foundation; either         -->
<!-- version 2.1 of the License, or (at your option) any later version;   -->
<!-- with the OCaml static compilation exception.                         -->
<!--                                                                      -->
<!-- This library is distributed in the hope that it will be useful,      -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of       -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    -->
<!-- Lesser General Public License for more details.                      -->
<!--                                                                      -->
<!-- You should have received a copy of the GNU Lesser General Public     -->
<!-- License along with this library; if not, write to the Free Software  -->
<!-- Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,               -->
<!-- MA 02110-1301, USA.                                                  -->
<!--                                                                      -->
<!-- Contact: gildor@debian.org                                           -->
<!--**********************************************************************-->


<refentry>

  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="refentryinfo.xml"/>

  <refmeta>
    <refentrytitle>MLDONKEY_USERS</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>
  
  <refnamediv>
    <refname>&dhprg;</refname>
  
    <refpurpose>a configuration editor for <command>mlnet</command> init file.</refpurpose>
  </refnamediv>
  
  <refsynopsisdiv>
    <cmdsynopsis>
      &dhprg; 
      <group choice="req">
        <arg>--add <replaceable>name</replaceable></arg>
        <arg>--del <replaceable>name</replaceable></arg>
        <arg>--check <replaceable>name</replaceable></arg>
        <arg>--list</arg>
        <arg>--test-users-section</arg>
        <arg>--dump-users-section</arg>
        <arg>--strip-users-section</arg>
      </group>
      <arg>-p <replaceable>password</replaceable></arg>
      <arg>-f <replaceable>file</replaceable></arg>
      <arg>-q</arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1>
    <title>DESCRIPTION</title>
  
    <para>This manual page documents briefly the &dhprg; command.</para>
  
    <para>This manual page was written for the &debian; distribution
      because the original program is &debian; specific.</para>
  
    <para>This script was written by the debian developer to enable easy
      user management configuration of <command>mlnet</command> server.</para>
  
    <variablelist>
      <varlistentry>
        <term><option>-f <replaceable>file</replaceable></option></term>
        <listitem>
          <para>This option is needed. This is the name
          of the file which contains the actual list of users
          (<filename>downloads.ini</filename>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-p <replaceable>password</replaceable></option></term>
        <listitem>
          <para>Set the password for this user. If not set through 
          the command line and required by action to be taken, user 
          will be prompted to enter one.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-q</option></term>
        <listitem>
          <para>Quiet mode.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--add <replaceable>name</replaceable></option></term>
        <listitem>
          <para>Add the user whose name is given. If no password is
          set, user will be prompted to enter one. If the user already 
          exists, he will be replaced by the new one.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--del <replaceable>name</replaceable></option></term>
        <listitem>
          <para>Delete the user whose name is given.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--check <replaceable>name</replaceable></option></term>
        <listitem>
          <para>Check that the user has the password set with -p. If no
          password is provided, it assumes that the application is performing
          a sanity check and return an error if the password of the corresponding
          user is empty.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--list</option></term>
        <listitem>
          <para>List all user managed by the file used.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--test-users-section</option></term>
        <listitem>
          <para>
            Check for the presence of a users section. If a users section is found
            in the given file, exit with an error code of 0 else exit with an error 
            code of 1.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--dump-users-section</option></term>
        <listitem>
          <para>
            Print on the standard output the users section.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--strip-users-section</option></term>
        <listitem>
          <para>
            Print on the standard output all the given file, without the users 
            section.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  
    <para>
      The action <option>--add</option>, <option>--del</option>, <option>--check</option>,
      <option>--list</option>, <option>--test-users-section</option>, 
      <option>--dump-users-section</option> and <option>--strip-users-section</option>
      are exclusive. You can only perform one at the same time.
    </para>
  
  </refsect1>
  
  <refsect1>
    <title>RETURN VALUE</title>
  
    <para>If the program cannot found the user, an error code of 1 is returned.</para>
  
    <para>During <option>--check</option>, if the password does not match the given password, 
    an error code of 1 is returned. If no password is given and that the password match with the 
    empty string, an error code of 1 is returned. Otherwise, it returns an error code of 0 ( no 
    error )</para>
  
    <para>
      During <option>--test-users-section</option>, if a section if found an
      error code of 0 is returned.  Otherwise an error code of 1 is returned.
    </para>
  
  </refsect1>
  
  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="license.xml"/>

  <refsect1>
    <title>SEE ALSO</title>

    <para>
      <citerefentry>  
        <refentrytitle>
          <command>mlnet</command>
        </refentrytitle>  
        <manvolnum>1</manvolnum>
      </citerefentry>
    </para>
 
  </refsect1>
  
</refentry>
